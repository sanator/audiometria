<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <title>Test de audiometría</title>
</head>
<body>
    <div class="container">
        <h1>Test de audiometría</h1>
        <hr>
        @if (isset($saved))
            <div class="alert alert-primary" role="alert">
                <strong>El diagnóstico ha sido guardado correctamente.</strong>
            </div>
        @endif
        @if (isset($diagnostic))
            @if ($diagnostic == '')
                <div class="row">
                    <div class="col-sm text-center">
                        <div class="alert alert-warning" role="alert">
                            El sistema aún no cuenta con suficientes resultados para poder dar un diagnóstico optimo por favor agregue el diagnóstico en el recuadro de abajo.
                        </div>
                        <form action="diagnostic" method="POST">
                            @csrf
                            <label for="diagnostic">Agregar diagnóstico</label>
                            <input type="text" class="form-control" id="diagnostic" name="diagnostic" placeholder="Ingresa el diagnóstico" required>
                            <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{$user_id}}" required>
                            <br>
                            <button type="submit" class="btn btn-primary">Guardar diagnóstico</button>
                        </form>
                    </div>
                </div>
            @else
                <div class="alert alert-primary" role="alert">
                    <strong>Tu diagnóstico es:</strong> {{$diagnostic}}
                </div>
            @endif
        @endif
        <form action="store" method="POST">
            @csrf
            @include('user_form')
            @include('questions_form')
            @include('audio_form')
            <br>
            <br>
            <button type="submit" class="btn btn-primary btn-lg btn-block">Guardar información</button>
            <br>
        </form>
    </div>
</body>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
<script type="text/babel" src="{{asset('js/audioTest.js')}}"></script>
</html>