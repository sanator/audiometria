@foreach($question_categories as $question_category)
    <hr>
    <h2>{{$question_category->name}}</h2>
    @foreach ($question_category->questions as $question)
        <label><b>{{$question->name}}</b></label>
        <div class="custom-control custom-radio custom-control-inline" style="float:right;">
            <input type="radio" id="question-{{$question->id}}-2" name="question-{{$question->id}}" value="0" class="custom-control-input" required>
            <label class="custom-control-label" for="question-{{$question->id}}-2">No</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline" style="float:right;">
            <input type="radio" id="question-{{$question->id}}-1" name="question-{{$question->id}}" value="1" class="custom-control-input" required>
            <label class="custom-control-label" for="question-{{$question->id}}-1">Si</label>
        </div>
        <br>
    @endforeach
@endforeach