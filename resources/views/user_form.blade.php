<h2>Información básica</h2>
<div class="row">
    <div class="form-group col">
        <label for="firstName">Primer Nombre</label>
        <input type="text" class="form-control" id="firstName" name="first_name" placeholder="Ingresa tu primer nombre" required>
    </div>
    <div class="form-group col">
        <label for="middleName">Segundo Nombre</label>
        <input type="text" class="form-control" id="middleName" name="middle_name" placeholder="Ingresa tu segundo nombre" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="firstLastName">Primer Apellido</label>
        <input type="text" class="form-control" id="firstLastName" name="primary_last_name" placeholder="Ingresa tu primer apellido" required>
    </div>
    <div class="form-group col">
        <label for="secondLastName">Segundo Apellido</label>
        <input type="text" class="form-control" id="secondLastName" name="second_last_name" placeholder="Ingresa tu segundo apellido" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="type_identification">Tipo de documento</label>
        <select class="custom-select" id="type_identification" name="type_identification">
            <option selected value="0">--- Selecciona ---</option>
            <option value="1">C.C.</option>
            <option value="2">T.I.</option>
            <option value="3">C.E.</option>
        </select>
    </div>
    <div class="form-group col">
        <label for="identification">Número de documento</label>
        <input type="number" class="form-control" id="identification" name="identification" placeholder="Ingresa tu número de documento" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="expeditionCity">Ciudad de expedición</label>
        <input type="text" class="form-control" id="expeditionCity" name="expedition_city" placeholder="Ingresa la ciudad de expedición de tu documento" required>
    </div>
    <div class="form-group col">
        <label for="nationality">Nacionalidad</label>
        <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Ingresa tu nacionalidad" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="placeOfBirth">Lugar de nacimiento</label>
        <input type="text" class="form-control" id="placeOfBirth" name="place_of_birth" placeholder="Ingresa la ciudad donde naciste" required>
    </div>
    <div class="form-group col">
        <label for="dateOfBirth">Fecha de nacimiento</label>
        <input type="date" class="form-control" id="dateOfBirth" name="date_of_birth" placeholder="Ingresa tu fecha de nacimiento" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="address">Dirección</label>
        <input type="text" class="form-control" id="address" name="address" placeholder="Ingresa tu dirección" required>
    </div>
    <div class="form-group col">
        <label for="phone">Teléfono</label>
        <input type="number" max="9999999999" min="1000000000" class="form-control" id="phone" name="phone" placeholder="Ingresa tu número telefónico" required>
    </div>
    <div class="form-group col">
        <label for="email">Correo</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Ingresa tu correo electrónico personal" required>
    </div>
</div>
<div class="row">
    <div class="form-group col">
        <label for="enterprise">Empresa</label>
        <input type="text" class="form-control" id="enterprise" name="enterprise" placeholder="Ingresa el nombre de la empresa en que trabajas" required>
    </div>
    <div class="form-group col">
        <label for="economy_activity">Actividad económica</label>
        <input type="text" class="form-control" id="economy_activity" name="economy_activity" placeholder="Ingresa tu actividad económica" required>
    </div>
</div>
