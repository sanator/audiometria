<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpertDiagnostic extends Model
{
    protected $fillable = [
        'user_id',
        'diagnostic'
    ];
}
