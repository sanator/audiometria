<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'primary_last_name',
        'second_last_name',
        'type_identification',
        'identification',
        'expedition_city',
        'nationality',
        'place_of_birth',
        'date_of_birth',
        'address',
        'phone',
        'email',
        'enterprise',
        'economy_activity'
    ];
}
