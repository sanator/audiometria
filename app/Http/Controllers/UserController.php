<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller {

    public function store(Request $request) {
        $user = new User;
        $user->first_name = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->primary_last_name = $request->primary_last_name;
        $user->second_last_name = $request->second_last_name;
        $user->type_identification = $request->type_identification;
        $user->identification = $request->identification;
        $user->expedition_city = $request->expedition_city;
        $user->nationality = $request->nationality;
        $user->place_of_birth = $request->place_of_birth;
        $user->date_of_birth = $request->date_of_birth;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->enterprise = $request->enterprise;
        $user->economy_activity = $request->economy_activity;
        $user->save();
    }
}