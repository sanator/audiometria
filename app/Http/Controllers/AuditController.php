<?php

namespace App\Http\Controllers;

use App\Question;
use App\Answer;
use App\User;
use App\ExpertDiagnostic;
use App\QuestionCategory;
use Illuminate\Http\Request;

class AuditController extends Controller
{

    private $validPercentage = 70;
    private $validQuantity = 2;
    public $user_id;

    public function index () {
        $question_categories = QuestionCategory::with('questions')->get();
        return view('base', ['question_categories' => $question_categories]);
    }

    public function store(Request $request) {
        $this->storeUser($request);
        $this->storeAnswer($request->all());
        $diagnostic = $this->getExpertDiagnostic($request->all());
        $question_categories = QuestionCategory::with('questions')->get();
        return view('base',
            ['question_categories' => $question_categories,
            'diagnostic' => $diagnostic,
            'user_id' => $this->user_id]);
    }

    private function storeUser($data) {
        $user = new User;
        $user->first_name = $data->first_name;
        $user->middle_name = $data->middle_name;
        $user->primary_last_name = $data->primary_last_name;
        $user->second_last_name = $data->second_last_name;
        $user->type_identification = $data->type_identification;
        $user->identification = $data->identification;
        $user->expedition_city = $data->expedition_city;
        $user->nationality = $data->nationality;
        $user->place_of_birth = $data->place_of_birth;
        $user->date_of_birth = $data->date_of_birth;
        $user->address = $data->address;
        $user->phone = $data->phone;
        $user->email = $data->email;
        $user->enterprise = $data->enterprise;
        $user->economy_activity = $data->economy_activity;
        $user->save();

        $this->user_id = $user->id;
    }

    private function storeAnswer($data) {
        foreach($data as $key => $value) {
            if($this->startsWith($key, 'question')) {
                $newKey = explode('-', $key)[1];
                $answer = new Answer;
                $answer->question_id = $newKey;
                $answer->user_id = $this->user_id;
                $answer->answer = $value;
                $answer->save();
            }
        }
    }

    private function getExpertDiagnostic($data) {
        $diagnostics = $this->getDiagnostics($data);

        if (empty($diagnostics)) {
            return '';
        }

        return $this->getDiagnostic($diagnostics);
    }

    private function calculatePercentage($value, $total) {
        return ($value / $total) * 100;
    }

    private function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function saveDiagnostic(Request $request) {
        $diagnostic = new ExpertDiagnostic;
        $diagnostic->user_id = $request->user_id;
        $diagnostic->diagnostic = $request->diagnostic;
        $diagnostic->save();

        $question_categories = QuestionCategory::with('questions')->get();
        return view('base', ['question_categories' => $question_categories, 'saved' => true]);
    }

    private function getDiagnostics($data) {
        $diagnostics = [];

        foreach($data as $key => $value) {
            if($this->startsWith($key, 'question')) {
                $newKey = explode('-', $key);
                $answers = Answer::where('question_id', $newKey[1])
                                    ->where('answer', $value)
                                    ->get();
                foreach($answers as $answer) {
                    $diagnosticObj = ExpertDiagnostic::where('user_id', $answer->user_id)->first();
                    if ($diagnosticObj) {
                        $diagnostics[] = $diagnosticObj->id;
                    }
                }
            }
        }

        return $diagnostics;
    }

    private function getDiagnostic($diagnostics) {
        $diagnosticsAverage = arsort(array_count_values($diagnostics));
        $totalDiagnostics = count($diagnostics);
        $diagnosticKey = array_key_first($diagnosticsAverage);

        if ($this->validQuantity <= $diagnosticsAverage[$diagnosticKey] &&
            $this->validPercentage <= $this->calculatePercentage($diagnosticsAverage[$diagnosticKey], $totalDiagnostics)) {
            $diagnostic = ExpertDiagnostic::find($diagnosticKey)->diagnostic;
            $storeDiagnostic = new ExpertDiagnostic;
            $storeDiagnostic->user_id = $this->user_id;
            $storeDiagnostic->diagnostic = $storeDiagnostic->diagnostic;
            $storeDiagnostic->save();
        } else {
            $diagnostic = '';
        }

        return $diagnostic;
    }
}
