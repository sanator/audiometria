<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question_category_id',
        'name',
        'with_options',
        'with_comment'
    ];
}
