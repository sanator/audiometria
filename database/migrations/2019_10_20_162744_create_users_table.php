<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            $table->string('first_name', 50);
            $table->string('middle_name', 50);
            $table->string('primary_last_name', 50);
            $table->string('second_last_name', 50);
            $table->string('type_identification', 3);
            $table->bigInteger('identification');
            $table->string('expedition_city', 50);
            $table->string('nationality', 50);
            $table->string('place_of_birth', 50);
            $table->date('date_of_birth');
            $table->string('address', 150);
            $table->bigInteger('phone');
            $table->string('email', 200);
            $table->string('enterprise', 100);
            $table->string('economy_activity', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
