<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAudioAnwersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audio_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('audio_id');
            $table->unsignedBigInteger('user_id');
            $table->string('answer');
            $table->timestamps();
            $table->foreign('audio_id')->references('id')->on('audios');
            $table->foreign('user_id')->references('id_user')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audio_answers');
    }
}
