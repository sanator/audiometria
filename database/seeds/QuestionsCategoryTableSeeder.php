<?php

use Illuminate\Database\Seeder;

class QuestionsCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_categories')->insert([
            ['name' => 'Antecedentes Otológicos'],
            ['name' => 'Antecedentes Personales']
        ]);
    }
}
