<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['question_category_id' => 1, 'name' => '¿Ha sufrido de dolor de oído?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Ha sufrido de vértigo?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Ha sufrido de otitis?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Ha sentido pitos o zumbidos en su oído?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Ha tenido derrame de sangre o líquidos de sus oídos?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Ha tenido algún accidente?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Ha tenido perforación timpánica?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Posee alguna malformación en alguno de los oídos? (Microtia, Agenesia, otros)', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Posee discriminación del habla? (Entiende mal cuando se le habla)', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 1, 'name' => '¿Oye una conversación corriente? (Entiende una conversación sin decir: ¿Qué?)', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Escucha música con audífonos?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Ha realizado polígono? (Protección auditiva)', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Se transporta en moto? (¿Désde que año y cuántos años?)', 'with_options' => true, 'with_comment' => true],
            ['question_category_id' => 2, 'name' => '¿Realiza algún tipo de deporte de contacto? (Fútbol, natación, ciclísmo)', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Sufre de diabétes?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Sufre de hipertensión?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Sufre de cancer?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Sufre de hipoglicemia?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Sufre de hipotiroidismo?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Sufre de sinocitis?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Sufre de rinitis?', 'with_options' => true, 'with_comment' => false],
            ['question_category_id' => 2, 'name' => '¿Ha estado expuesto a ruido laboral? (¿Cuántas horas y cuántos años?)', 'with_options' => true, 'with_comment' => true],
            ['question_category_id' => 2, 'name' => '¿Usa protección auditiva? ¿De qué tipo? (Inserción, silicona o doble protección auditiva)', 'with_options' => true, 'with_comment' => true],
        ]);
    }
}
