class AudioButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            audio: new Audio('audio/3000.wav'),
            play: "Reproducir"
        };
        this.playAudio = this.playAudio.bind(this);
        this.lowVolume = this.lowVolume.bind(this);
        this.upVolume = this.upVolume.bind(this);
    }

    componentDidMount() {
    }

    render() {
        this.state.audio.volume = 0.2;

        return (
            <div class="row">
                <div class="col-sm">
                    <button type="button" class="btn btn-info" onClick={this.lowVolume} disabled={this.state.play === "reproduciendo"}>Escuché</button>
                </div>
                <div class="col-sm text-center">
                    <button type="button" class="btn btn-primary" onClick={this.playAudio} disabled={this.state.play === "reproduciendo"}>{this.state.play}</button>
                </div>
                <div class="col-sm">
                    <button type="button" class="btn btn-danger float-right" onClick={this.upVolume} disabled={this.state.play === "reproduciendo"}>No escuché</button>
                </div>
            </div>
        );
    }

    playAudio() {
        this.state.audio.play();
        this.state.play = "reproduciendo";
        this.state.audio.onended = function () {
            this.state.play = "Reproducir";
        }.bind(this);
    }

    lowVolume() {
        this.state.audio.volume -= 0.05;
    }

    upVolume() {
        this.state.audio.volume += 0.1;
    }
}

ReactDOM.render(
    <AudioButton />,
    document.getElementById('AudioTest')
);